import math

def cross_hatch(segments, spacing, angle):
    theta = angle * math.pi / 180.
    rotation = complex(math.cos(-theta), math.sin(-theta))
    
    lines = []
    for segment in segments:
        for i in range(1,len(segment)):
            lines.append((segment[i-1]*rotation, segment[i]*rotation))
    
    # after rotation, our cross-hatch is horizontal
    
    minX = min( min(line[0].real, line[1].real) for line in lines )
    minY = min( min(line[0].imag, line[1].imag) for line in lines )    
    maxX = max( max(line[0].real, line[1].real) for line in lines )
    maxY = max( max(line[0].imag, line[1].imag) for line in lines )        
    
    # avoid horizontals