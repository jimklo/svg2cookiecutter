# OpenSCAD cookie cutter file output for Inkscape

This was a project that originated from [Alexander Pruss](https://alexanderpruss.blogspot.com/2016/12/3d-printable-cookie-cutters-with.html) where the original distribution is available [on the Inkscape Resources](https://inkscape.org/en/~arpruss/★openscad-cookie-cutter-file-output).

I wanted to make some custom cookie cutters, but found that Alexander's extension was lacking in a few features, namely 'holes' in feature elements within the perimeter of the cookie shape as it doesn't work with polygon fill.  After looking at the `SCAD` output generated, I realized it shouldn't be too difficult to support fills and some basic boolean operations.

Enhancements include:
* a not-so-super-efficient enhanced version to support filled polygons with holes.
* a more verbosely documented SCAD output to simplify manual editing.


## Installation

1. Install Python as per your OS enviroment
2. Install Inkscape (Tested with 0.92.3 on macOS 10.13.6)
3. Copy the contents of this repository into `${HOME}/.config/inkscape/extensions` or whatever location you have defined in Inkscape's *System Preferences* for *User extensions* or *Inkscape extenstions*.
4. Relaunch Inkscape if it was running.
5. When using the *Save as...* or *Save a copy...* within Inkscape, you should be able to select **OpenSCAD cookie cutter file (*.scad)** from the file dialog options.

## Use

[Alexander's Instructable](https://www.instructables.com/id/3D-Printable-Cookie-Cutters-With-Inkscape-and-Open/) defines the basics with some good detail.  To summarize when creating vector artwork in SVG:

* Red polylines should be used for the exterior perimeter
* Black polylines should be used for interior features to create impressions in the cookie surface.
* Yellow polylines will create a hole within an interior filled polylines (useful for script lettering with holes - e.g. a, e, o, b, d, and etc...).
* Green polylines create interior walls to cut all the way through.
* Blue polylines will create a surface to support floating elements.

Ensure you've converted your artwork to paths, and remove strokes and fills as needed.  If you're using a fill, you should remove the stroke to prevent duplicate geometry from being created.

Once your artwork is ready within Inkscape, choose *Save a copy..*, supply a file name and location, and select the file type as **OpenSCAD cookie cutter file (*.scad)**, then click *Save*.  You can then open this file within OpenSCAD to further edit and produce an `STL` file.


## License

[The MIT License](https://opensource.org/licenses/MIT)